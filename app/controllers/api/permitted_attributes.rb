# frozen_string_literal: true

module Api
  module PermittedAttributes
    ATTRIBUTES = [
      :task_attributes
    ].freeze

    mattr_reader(*ATTRIBUTES)

    @@task_attributes = [
      :avatar_url, :description, :recorded_on
    ]
  end
end
