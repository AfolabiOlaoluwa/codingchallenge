# frozen_string_literal: true

module Api
  module V1
    class TasksController < ApiController
      include StrongParameters

      def index
        @task = Task.all.load
        render json: @task
      end

      def create
        @task = Task.create!(create_action_params)

        if @task
          render json: @task
        else
          render json: @task.errors
        end
      end

      def update
        @task = Task.find(params[:id])

        @task.update_attribute(:recorded_on, Time.now)
        render json: @task if @task.save
      end

      private

      def create_action_params
        params.require(:task).permit(permitted_task_attributes)
      end
    end
  end
end
