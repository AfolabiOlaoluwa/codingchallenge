# frozen_string_literal: true

module Api
  module V1
    module StrongParameters
      def permitted_attributes
        PermittedAttributes
      end

      delegate(*PermittedAttributes::ATTRIBUTES,
               to: :permitted_attributes,
               prefix: :permitted)

      def permitted_task_attributes
        permitted_attributes.task_attributes
      end
    end
  end
end
