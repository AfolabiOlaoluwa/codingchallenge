# frozen_string_literal: true

class ApiController < ApplicationController
  before_action :set_api_request

  private

  def set_api_request
    request.format = request.format == :xml ? :xml : :json
  end
end
