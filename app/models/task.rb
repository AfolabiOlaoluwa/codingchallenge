# frozen_string_literal: true

class Task < ApplicationRecord
  validates :avatar_url, presence: true
  validates :description, presence: true
end
