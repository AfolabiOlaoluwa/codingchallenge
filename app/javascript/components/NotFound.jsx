import React from 'react';
import { Link } from 'react-router-dom';

class NotFound extends React.Component {
    render() {
        const notFound = (
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="text-center mt-4">
                                <img className="img-fluid p-4" src="https://sb-admin-pro.startbootstrap.com/assets/img/illustrations/404-error.svg" alt="404 Image"/>
                                <div>
                                    <h1>The page you were looking for doesn't exist.</h1>
                                    <p className="lead">You may have mistyped the address or the page may have moved.</p>
                                </div>
                                <p className="lead">If you are the application owner check the logs for more information.</p>
                                <Link to="/" className="btn btn-link mt-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                                         strokeLinejoin="round" className="feather feather-arrow-left ml-0 mr-1">
                                        <line x1="19" y1="12" x2="5" y2="12"/>
                                        <polyline points="12 19 5 12 12 5"/>
                                    </svg>
                                    Return To Tasks
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
        return (
            <>
                {notFound}
            </>
        );
    }
}

export default NotFound;

