import React from "react";
import { Link } from "react-router-dom";
import moment from 'moment'

class Tasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: []
        };
    }

    componentDidMount() {
        const url = "/api/v1/tasks/index";
        fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Network response was not ok.");
            })
            .then(response => this.setState({ tasks: response }))
            .catch(() => this.props.history.push("/"));
    }

    handleCheck(event, task) {
        const element = event.target;
        if (element.checked) {
            const url = '/api/v1/tasks/'+task.id+'';
            const { recorded_on } = this.state;

            if (task.recorded_on !== null )
                return;

            const body = {
                recorded_on
            };

            const csrf = document.querySelector('meta[name="csrf-token"]').content;
            fetch(url, {
                method: "PATCH",
                headers: {
                    "X-CSRF-Token": csrf,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            })
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw new Error("Network response was not ok.");
                })
                .then(() => window.location.reload(false))
                .catch(error => console.log(error.message));
        }
    }

    render() {
        const {tasks} = this.state;
        const allTasks = tasks.map((task, index) => <div key={index} className="col-sm-12 col-md-12 col-lg-12">
            <div className="row">
                <div className="col-sm-12 col-xl-12 col-xxl-12">
                    <div className="card mb-2">
                        <div className="card-body">
                            <div className="d-flex align-items-center justify-content-between">
                                <div className="d-flex align-items-center flex-shrink-0 mr-3">
                                    <div className="avatar avatar-xl mr-3 bg-gray-200">
                                        <img src={task.avatar_url} className="avatar-img img-fluid" alt={`${task.description} image`}/>
                                    </div>
                                    <div className="d-flex flex-column font-weight-bold">
                                        <a className="text-dark line-height-normal mb-1" href="#!">Username</a>
                                        <div className="small text-muted line-height-normal truncate">
                                            {task.description}
                                        </div>
                                    </div>
                                </div>
                                <div className="pb-2">
                                    { task.recorded_on !== null ?
                                        <p>{moment(task.recorded_on).format("LT")}</p>
                                        :
                                        <div className="checkBoxContainer">
                                            <input
                                                type="checkbox"
                                                className="checkBox"
                                                onChange={(event) => this.handleCheck(event, task)}
                                            />
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
        const noTask = <div className="vw-100 vh-50 d-flex align-items-center justify-content-center">
            <h4>
                No tasks yet. <Link to="/new_task">Kindly create a task</Link>
            </h4>
        </div>;

        return (
            <>
                <nav className="navbar navbar-expand-lg navbar-dark task-navbar-color">
                    <div className="container">
                        <div className="navbar-header">
                            <div className="navbar-brand">Task</div>
                        </div>
                        <div>
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <Link to="/new_task">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor"
                                             className="bi bi-plus text-white" viewBox="0 0 16 16">
                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                        </svg>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="py-5">
                    <main className="container">
                        <div className="row">
                            {tasks.length > 0 ? allTasks : noTask}
                        </div>
                    </main>
                </div>
            </>
        );
    }
}
export default Tasks;