import React from "react";
import {Link} from "react-router-dom";

class NewTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar_url: "",
            description: ""
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.stripHtmlEntities = this.stripHtmlEntities.bind(this);
    }

    stripHtmlEntities(str) {
        return String(str)
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;");
    }

    onChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    onSubmit(event) {
        event.preventDefault();
        const url = "/api/v1/tasks/create";
        const { avatar_url, description } = this.state;

        if (avatar_url.length === 0 || description.length === 0 )
            return;

        const body = {
            avatar_url,
            description: description.replace(/\n/g, "<br> <br>")
        };

        const token = document.querySelector('meta[name="csrf-token"]').content;
        fetch(url, {
            method: "POST",
            headers: {
                "X-CSRF-Token": token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Network response was not ok.");
            })
            .then(() => this.props.history.push(`/`))
            .catch(error => console.log(error.message));
    }

    render() {
        return (
            <>
                <nav className="navbar navbar-expand-lg navbar-dark task-navbar-color">
                    <div className="container">
                        <div className="navbar-header">
                            <div className="navbar-brand">Add Task</div>
                        </div>
                        <div>
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <Link to="/">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor" className="bi bi-arrow-left text-white" viewBox="0 0 16 16">
                                            <path fillRule="evened" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                                        </svg>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-sm-12 col-lg-6 offset-lg-3">
                            <h1 className="font-weight-normal mb-5">
                                Add a new task to our awesome task collection.
                            </h1>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label htmlFor="avatar_url">Avatar URL</label>
                                    <input
                                        type="url"
                                        name="avatar_url"
                                        value={this.state.value}
                                        className="form-control"
                                        required
                                        onChange={this.onChange}
                                    />
                                </div>
                                <label htmlFor="description">Task Description</label>
                                <textarea
                                    className="form-control"
                                    name="description"
                                    value={this.state.value}
                                    rows="5"
                                    required
                                    onChange={this.onChange}
                                />
                                <button type="submit" value="Save" className="btn btn-primary mt-3">
                                    Add Task
                                </button>
                                <Link to="/" className="btn btn-link mt-3">
                                    Back to tasks
                                </Link>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default NewTask;