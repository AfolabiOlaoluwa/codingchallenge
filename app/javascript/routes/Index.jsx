import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Tasks from "../components/Tasks";
import NewTask from "../components/NewTask";
import NotFound from "../components/NotFound";

export default <Router>
  <Switch>
    <Route path="/" exact component={Tasks} />
    <Route path="/new_task" exact component={NewTask} />
    <Route path="/*" exact component={NotFound} />
  </Switch>
</Router>;