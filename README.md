# README

##### Ruby Version

* `ruby 2.7.2p137 (2020-10-01 revision 5445e04352) [x86_64-darwin19]`

##### Rails Version

* `Rails 6.1.3.1`

##### Setup Instruction/Configuration

I will advise you use `Chruby` to configure your environment with `Ruby and Rails Versions` I used for this solution before moving forward with the steps below. You can follow `https://medium.com/@hpux/using-chruby-as-the-default-ruby-version-manager-c11346e3cc` for that. When you are done, then you can proceed with the following steps:
1. Clone the repo.
2. Run `EDITOR=vim rails credentials:edit`. This creates `credentials.yml.enc` and `master.key` file for you inside `/config`. For more hint see https://gist.github.com/db0sch/19c321cbc727917bc0e12849a7565af9.
3. Run `bundle install` from the root of the application.
4. Run `rails db:create`
5. Run `rails db:migrate`
6. Run `rails db:seed` to put some data in the database.
7. Then start the app by running `rails s --binding=127.0.0.1`.

By now the app should be up and running and you can check by going to port Listening on http://127.0.0.1:3000 on your browser.

##### Running Tests

From the root of the application, run `bundle exec rspec`.

NOTE
----

I created a **One-Tier Architecture** application and here is my approach below:

I approached this solution in a way to allow app users to clock in multiple events in a day. Also, the post's time is set at backend once you hit the checkbox so that users don't have access to edit the time themselves. I believe giving that grace for the app users to edit post time means that app users are permitted to input discrepancies in events, which in turns can have a severe authenticity issue, and I do not believe apps are made that way.

There are tons of improvement if more time is awarded and I will leave discussions on this to the follow-up interview since I intentionally leave them out in my solution so as to buy more time in submission. Example are:
- RSpec Request Spec extension to capture more tests 
- ReactJS Testing even though I wrote  request spec for the ReactJS endpoints in the solution.
- API Authentication. This will make the app limit details to a specific users. 
- API Security such as using Knock Gem for JWT. 
- Rails API Encapsulation. Though I tried encapsulation params to have a single holding for entire system

So there are still a lot of improvement to made if I could step beyond the allocated time for this task and a lot I believe organisational structure can fix as well. I will leave the rest of the discussion to follow-up interview.
