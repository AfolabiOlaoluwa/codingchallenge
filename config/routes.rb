# frozen_string_literal: true

Rails.application.routes.draw do
  root 'homepage#index'
  get '/new_task', to: 'homepage#index'

  namespace :api do
    namespace :v1 do
      get 'tasks/index'
      post 'tasks/create'
      patch 'tasks/:id', to: 'tasks#update', as: 'tasks_update'
    end
  end

  get '/*path', to: 'homepage#index', constraints: lambda { |request|
    !request.xhr? && request.format.html?
  }
end
