# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    avatar_url { Faker::Avatar.image }
    description { Faker::Lorem.sentence }
    recorded_on { Faker::Time }
  end
end
