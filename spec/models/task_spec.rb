# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:subject) { build(:task) }
  let(:task) { FactoryBot.create(:task) }

  it 'is valid from the factory' do
    expect(subject).to be_valid
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:avatar_url) }
    it { is_expected.to validate_presence_of(:description) }
  end

  it 'is not valid without a avatar_url' do
    subject.avatar_url = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid without a description' do
    subject.description = nil
    expect(subject).to_not be_valid
  end

  it 'is valid without a recorded_on' do
    subject.recorded_on = nil
    expect(subject).to be_valid
  end
end
