# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Homepage', type: :request do
  describe '#index' do
    it 'should returns a 200 response' do
      get root_path
      expect(response).to have_http_status '200'
    end

    it 'should render homepage#index template' do
      get root_path
      expect(response).to render_template 'homepage/index', 'layouts/application'
    end
  end

  describe '#new_task' do
    it 'should returns a 200 response' do
      get new_task_path
      expect(response).to have_http_status '200'
    end

    it 'renders new form' do
      get new_task_path
      expect(response).to render_template 'homepage/index', 'layouts/application'
    end
  end
end
