# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::TasksController, type: :request do

  describe '#index' do
    it 'should returns a 200 response' do
      get api_v1_tasks_index_path
      expect(response).to have_http_status :success
    end
  end

  describe '#create' do
    it 'should returns a 200 response' do
      get api_v1_tasks_create_path
      expect(response).to have_http_status :success
    end

    it 'creates a task' do
      post api_v1_tasks_create_path, params: {
        task: {
          avatar_url: 'https://gitlab.com/uploads/-/system/user/avatar/5580933/avatar.png?width=40',
          description: 'Meeting time'
        }, format: :json
      }
      expect(response).to have_http_status :success
    end

    it 'changes task by 1' do
      expect do
        post api_v1_tasks_create_path, params: {
          task: {
            avatar_url: 'https://gitlab.com/uploads/-/system/user/avatar/5580933/avatar.png?width=40',
            description: 'Meeting time'
          }, format: :json
        }
      end.to change(Task, :count).by(1)
    end
  end

  describe '#update' do
    before do
      @task = Task.create(
        avatar_url: 'https://gitlab.com/uploads/-/system/user/avatar/5580933/avatar.png?width=40',
        description: 'Nicholas is a good Team Lead'
      )
    end

    it 'should update recorded_on attribute with datetime' do
      patch api_v1_tasks_update_path(@task.id), params: {
        task: {
          recorded_on: Time.now
        }, format: :json
      }
      expect(response).to have_http_status :success
    end
  end
end
