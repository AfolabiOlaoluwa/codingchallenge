# frozen_string_literal: true

class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :avatar_url
      t.text :description
      t.datetime :recorded_on
    end
    add_index :tasks, %i[avatar_url description recorded_on]
  end
end
