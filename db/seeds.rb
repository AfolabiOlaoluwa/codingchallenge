tasks = [
  {
    avatar_url: "https://i.pravatar.cc/150?img=1",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet semper enim",
    recorded_on: 10.minutes.ago
  },
  {
    avatar_url: "https://i.pravatar.cc/150?img=2",
    description: "Phasellus maximus justo magna, eu pharetra nulla condimentum in. Aenean neque ligula, commodo",
    recorded_on: Time.now
  },
]

tasks.each do |record|
  Task.create!(record)
end
